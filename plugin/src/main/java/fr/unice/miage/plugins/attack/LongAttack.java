package fr.unice.miage.plugins.attack;

import java.awt.Color;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import fr.unice.miage.plugins.Plugin;

/**
 * Interface qui represente les plugins d'attaque
 */
@Plugin(libelle = "Attack")
public class LongAttack {

	int damage = 20;
	int energy = 30;

	/**
	 * Le robot qui utilise cette methode attaque le robot le plus proche de lui
	 * Meme s'ils ne sont pas cote a cote, ils s'attaquent quand meme
	 * 
	 * @param g
	 * @param listRobot
	 * @param unRobot
	 */
	public void attackRobot(Graphics g, ArrayList<Object> listRobot, Object unRobot) {
		try {
			System.out.println("BasicAttack.attackRobot()");
			// verification que le robot est en vie
			boolean alive = (boolean) unRobot.getClass().getMethod("isAlive").invoke(unRobot);
			if (alive) {
				//probabilite que le robot attaque ou rate son attaque
				int luck = (int) (Math.random() * 100);
				if (luck > 50) {
					// verification que le robot a de l'energie pour attaquer
					int energyBase = (int) unRobot.getClass().getMethod("getEnergy").invoke(unRobot);
					if (energyBase > this.energy) {
						int x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
						int y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
						Color color = (Color) unRobot.getClass().getMethod("getColor").invoke(unRobot);
						int x2;
						int y2;
						Color color2;
						int diffRobot;
						int diffMin = 999;
						Object robotProche = null;
						// Boucle qui cherche le robot le plus proche dans la list avec le robot en
						// parametre
						for (Object r : listRobot) {
							x2 = (int) r.getClass().getMethod("getX").invoke(r);
							y2 = (int) r.getClass().getMethod("getY").invoke(r);
							color2 = (Color) r.getClass().getMethod("getColor").invoke(r);
							// Verification que le robot n'est pas lui meme
							if (x != x2 && y != y2 && color != color2) {
								diffRobot = (int) unRobot.getClass()
										.getMethod("calculateDistanceWithRobot", Object.class).invoke(unRobot, r);
								// choisi le premier robot par defaut puis le robot le plus proche
								if (robotProche == null || (diffRobot < diffMin)) {
									diffMin = diffRobot;
									robotProche = r;
								}
							}
						}
						// Retire de l'energie au robot qui attaque et retire de la vie au robot qui
						// recoit l'attaque
						unRobot.getClass().getMethod("decrementEnergy", int.class).invoke(unRobot, this.energy);
						robotProche.getClass().getMethod("decrementHealth", int.class).invoke(robotProche, this.damage);
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}

}
