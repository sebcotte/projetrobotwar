package fr.unice.miage.plugins;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * Cette interface represente un plugin de base dans notre jeu
 * Chaque plugin doit implementer les methodes de cette interface
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Plugin {

	/**
	 * Obtient le libelle à afficher dans les menus ou autre pour le plugins
	 * @return Le libelle sous forme de String. Ce libelle doit etre clair et comprehensible facilement 
	 */
	public String libelle();
	
}