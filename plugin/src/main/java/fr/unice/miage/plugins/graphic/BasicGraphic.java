package fr.unice.miage.plugins.graphic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

@Plugin(libelle = "Graphic")
public class BasicGraphic extends JPanel{

	private static final long serialVersionUID = 8831773873385591633L;

	/**
	 * Dessiner un robot a l'ecran avec x et y du robot
	 * 
	 * @param g
	 * @param unRobot
	 */
	public void drawRobot(Graphics g, Object unRobot) {
		System.out.println("BasicGraphic.drawRobot()");
		try {
			int x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
			int y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
			this.drawRobotFinal(g, unRobot, x, y);
		} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Dessiner un robot a l'ecran avec une postion aléatoire
	 * 
	 * @param g
	 */
	public void drawRobotRandom(Graphics g, Object unRobot) {
		try {
			int x = (int) ((Math.random() * (610 - 10)) + 10); // depart du robot x
			int y = (int) ((Math.random() * (650 - 70)) + 70); // depart du robot y
			this.drawRobotFinal(g, unRobot, x, y);
		} catch (IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Dessiner un robot a l'ecran avec une postion
	 * 
	 * @param g
	 */
	public void drawRobotFinal(Graphics g, Object unRobot, int x, int y) {
		try {
			Color color = (Color) unRobot.getClass().getMethod("getColor").invoke(unRobot);
			Rectangle r = new Rectangle(x, y, 50, 50);
			g.setColor(color);
			g.fillRect((int) r.getX(), (int) r.getY(), (int) r.getWidth(), (int) r.getHeight());

			unRobot.getClass().getMethod("setX", int.class).invoke(unRobot, x);
			unRobot.getClass().getMethod("setY", int.class).invoke(unRobot, y);
		} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
}