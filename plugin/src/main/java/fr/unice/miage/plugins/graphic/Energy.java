package fr.unice.miage.plugins.graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

@Plugin(libelle = "Graphic")
public class Energy extends JPanel {

	private static final long serialVersionUID = -1357477064779282928L;

	/**
	 * Draw energy on screen
	 * 
	 * @param g
	 * @param unRobot
	 */
	public void drawEnergy(Graphics g, Object unRobot) {
		System.out.println("Energy.drawEnergy()");
		try {
			int energy = (int) unRobot.getClass().getMethod("getEnergy").invoke(unRobot);
			int x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
			int y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
			Color color = (Color) unRobot.getClass().getMethod("getColor").invoke(unRobot);
			g.setColor(color);
			g.setFont(new Font("default", Font.BOLD, 20));
			g.drawString("Energy : " + energy, x-30, y-30);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}
}
