package fr.unice.miage.plugins.movement;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

/**
 * Ce plugin permet au robot de trouver de l'energie ou de la vie avec de la
 * chance
 *
 */
@Plugin(libelle = "Movement")
public class LuckyMovement extends JPanel {

	private static final long serialVersionUID = -4208338046012029912L;

	/**
	 * donne aleatoirement de l'énergie, de la vie ou rien au robot
	 * 
	 * @param unRobot
	 */
	public void findEnergy(Object unRobot) {
		System.out.println("LuckyMovement.findEnergy()");
		try {
			// verification que le robot est en vie
			boolean alive = (boolean) unRobot.getClass().getMethod("isAlive").invoke(unRobot);
			if (alive) {
				int luck = (int) (Math.random() * 100);
				int nombreAleaVie = 20 + (int) (Math.random() * ((30 - 20) + 1));
				int nombreAleaEnergie = 50 + (int) (Math.random() * ((60 - 50) + 1));
				if (luck > 50 && luck < 70) {
					unRobot.getClass().getMethod("incrementHealth", int.class).invoke(unRobot, nombreAleaVie);
				} else if (luck >= 70) {
					unRobot.getClass().getMethod("incrementEnergy", int.class).invoke(unRobot, nombreAleaEnergie);
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}

}
