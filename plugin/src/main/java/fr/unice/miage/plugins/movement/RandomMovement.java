package fr.unice.miage.plugins.movement;

import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

/**
 * Ce plugin permet au robot de se deplacer de maniere aleatoire
 *
 */
@Plugin(libelle = "Movement")
public class RandomMovement extends JPanel {

	private static final long serialVersionUID = -4208338046012029912L;

	private int valMin = 110;
	private int valMax = 600;
	private int energyDeplacement = 5;
	
	/**
	 * Deplace le robot aleatoirement sur la fenetre
	 * 
	 * @param g
	 * @param unRobot
	 */
	public Object seDeplacer(Graphics g, Object unRobot) {
		System.out.println("RandomMovement.seDeplacer()");
		try {
			// verification que le robot est en vie
			boolean alive = (boolean) unRobot.getClass().getMethod("isAlive").invoke(unRobot);
			if (alive) {
				// verification que le robot a de l'energie pour se deplacer
				int energyBase = (int) unRobot.getClass().getMethod("getEnergy").invoke(unRobot);
				if (energyBase > this.energyDeplacement) {
					int nb1 = (int) (Math.random() * 200); // choisi si on change x ou y
					int nb2 = (int) (Math.random() * 40); // choisi de combien on change x ou y
					int x;
					int y;
					// On met a jour les coordonnees du robot
					if (nb1 < 50) {
						x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
						x = this.verifyCoord(x);
						unRobot.getClass().getMethod("setX", int.class).invoke(unRobot, x + nb2);
					} else if (nb1 >= 50 && nb1 < 100) {
						y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
						y = this.verifyCoord(y);
						unRobot.getClass().getMethod("setY", int.class).invoke(unRobot, y + nb2);
					} else if (nb1 >= 100 && nb1 < 150) {
						x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
						x = this.verifyCoord(x);
						unRobot.getClass().getMethod("setX", int.class).invoke(unRobot, x - nb2);
					} else if (nb1 > 150) {
						y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
						y = this.verifyCoord(y);
						unRobot.getClass().getMethod("setY", int.class).invoke(unRobot, y - nb2);
					}
					unRobot.getClass().getMethod("decrementEnergy", int.class).invoke(unRobot, this.energyDeplacement);
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
		return unRobot;
	}

	public int verifyCoord(int a) {
		a = (a > valMin) ? a : valMin;
		a = (a < valMax) ? a : valMax;
		return a;
	}

}
