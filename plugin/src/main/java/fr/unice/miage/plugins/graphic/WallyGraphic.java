package fr.unice.miage.plugins.graphic;

import java.awt.Color;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

@Plugin(libelle = "Graphic")
public class WallyGraphic extends JPanel {
	
	private static final long serialVersionUID = -677966293167285080L;

	/**
	 * Dessiner un robot a l'ecran avec x et y du robot
	 * 
	 * @param g
	 * @param unRobot
	 */
	public void drawRobot(Graphics g, Object unRobot) {
		System.out.println("MediumGraphic.drawRobot()");
		try {
			int x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
			int y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
			this.drawRobotFinal(g, unRobot, x, y);
		} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Dessiner un robot a l'ecran avec une postion aléatoire
	 * 
	 * @param g
	 */
	public void drawRobotRandom(Graphics g, Object unRobot) {
		try {
			int x = (int) ((Math.random() * (610 - 10)) + 10); // depart du robot x
			int y = (int) ((Math.random() * (650 - 70)) + 70); // depart du robot y
			this.drawRobotFinal(g, unRobot, x, y);
		} catch (IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Dessiner un robot a l'ecran avec une postion
	 * 
	 * @param g
	 */
	public void drawRobotFinal(Graphics g, Object unRobot, int x, int y) {
		try {
			Color color = (Color) unRobot.getClass().getMethod("getColor").invoke(unRobot);

			g.setColor(color);
			g.fillRect(x+10, y+10, 40, 40);// body
			g.fillRect(x-5, y+30, 15, 30);// feet
			g.fillRect(x+50, y+30, 15, 30);// feet
			g.fillRect(x+27, y-8, 5, 20);// bottomHead
			g.fillOval(x+2, y-15, 25, 15);// eyes
			g.fillOval(x+31, y-15, 25, 15);// eyes

			unRobot.getClass().getMethod("setX", int.class).invoke(unRobot, x);
			unRobot.getClass().getMethod("setY", int.class).invoke(unRobot, y);
		} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
}