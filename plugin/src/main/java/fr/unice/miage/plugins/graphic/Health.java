package fr.unice.miage.plugins.graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;

import fr.unice.miage.plugins.Plugin;

@Plugin(libelle = "Graphic")
public class Health extends JPanel{

	private static final long serialVersionUID = -6486331504290289395L;

	/**
	 * Draw health on screen
	 * 
	 * @param g
	 * @param unRobot
	 */
	public void drawHealth(Graphics g, Object unRobot) {
		System.out.println("Health.drawHealth()");
		try {
			int health = (int) unRobot.getClass().getMethod("getHealth").invoke(unRobot);
			int x = (int) unRobot.getClass().getMethod("getX").invoke(unRobot);
			int y = (int) unRobot.getClass().getMethod("getY").invoke(unRobot);
			Color color = (Color) unRobot.getClass().getMethod("getColor").invoke(unRobot);
			g.setColor(color);
			g.setFont(new Font("default", Font.BOLD, 20));
			g.drawString("Health : " + health, x-30, y-50);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}
}
