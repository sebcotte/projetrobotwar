package plugins.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import plugins.names.EGraphicPluginsNames;

/**
 * 
 * L'annotation GraphicPlugin permettra de marquer une classe et d'indiquer qu'elle est un plugin de type Graphic
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) // cette annotation marquera des classes uniquement

public @interface GraphicPlugin {

	EGraphicPluginsNames pluginType();
}
