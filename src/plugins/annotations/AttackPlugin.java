package plugins.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import plugins.names.EGraphicPluginsNames;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) // cette annotation marquera des classes uniquement

public @interface AttackPlugin {

	EAttackPluginsNames pluginType();
}
