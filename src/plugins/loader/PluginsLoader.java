package plugins.loader;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.JarFile;

import plugins.AttackPlugins;
import plugins.BasicGraphicPlugin;
import plugins.MovePlugin;
import plugins.MovementPlugins;
import plugins.annotations.AttackPlugin;
import plugins.annotations.GraphicPlugin;
import plugins.names.EGraphicPluginsNames;

/**
 * Cette classe g�re le chargemnt de n'importe quel plugin
 *
 */
public class PluginsLoader {
	
	private ArrayList<String> filesNames; // fichiers � charger

	// on enregistre les plugins dans des listes diff�rentes pour apr�s �viter de charger plusieurs fois le m�me plugin
	private ArrayList<Class<?>> classAttackPlugins; 
	private ArrayList<Class<?>> classGraphicPlugins;//Class<?> indique qu'on ne connait pas le type que l'objet Class va modeliser
	private ArrayList<Class<?>> classMovementPlugins;
	
	/*public static void main(String args[]){
		ArrayList<String> files = new ArrayList<String>();
		PluginsLoader pl = new PluginsLoader();
		
		// Remplir le tab "files"
		files.add("bin/plugins/BasicGraphicPlugin.class");
		
		pl.setFiles(files);
		System.out.println("File : " + pl.getFiles().get(0).toString());
		
		try {
			ArrayList<Class<?>> g = pl.loadGraphicPlugin();
			
			g.get(0).drawRobot(null);
			
		} catch (Exception e) {
			System.out.println("Go fuck " );
			e.printStackTrace();
		}
	}*/
	
	
	/**
	 * Get a plugin instance from a list
	 * @return A plugin instance 
	 */
	public HashMap<EGraphicPluginsNames, Class<?>> loadAGraphicPlugin(EGraphicPluginsNames pluginType){
		
		System.out.println("PluginsLoader.loadAGraphicPlugin()");
		// Parcourir la liste de plugins Graphic		
		for(Class<?> aPlugin : classGraphicPlugins){
			if( aPlugin.getAnnotation(GraphicPlugin.class) != null ) {
				
			}
			Annotation[] annotations = aPlugin.getAnnotations();
			for(Annotation annot : annotations){
				if(annot.getClass().equals(GraphicPlugin.class)){
					// Le plugin porte bien l'annotation GraphicPlugin
					
					if(((GraphicPlugin)annot).pluginType().equals(pluginType)) {	
						if(pluginType.equals(EGraphicPluginsNames.BASIC_GRAPHIC)) {
							
							
							BasicGraphicPlugin b = aPlugin;
							
							
						}
							
						HashMap<EGraphicPluginsNames, Class<?>> h = new HashMap<EGraphicPluginsNames, Class<?>>();
						h.put(pluginType, aPlugin);
						return h;
					}
					else{
						return new HashMap<EGraphicPluginsNames, Class<?>>();
					}
				}
				else{
					return new HashMap<EGraphicPluginsNames, Class<?>>();
				}
			}		
		}
		return new HashMap<EGraphicPluginsNames, Class<?>>();
	}
	
	public Class<?> loadAnAttackPlugin(){
		
	}
	
	
	/**
	 * Constructeur par d�faut
	 *
	 */
	public PluginsLoader(){
		this.classAttackPlugins = new ArrayList<Class<?>>();
		this.classGraphicPlugins = new ArrayList<Class<?>>();
		this.classMovementPlugins = new ArrayList<Class<?>>();
	}
	
	/**
	 * Constructeur pour indiquer les fichiers � charger
	 * @param files Tableau qui contient les fichiers � charger
	 */
	public PluginsLoader(ArrayList<String> files){
		this();
		this.filesNames = files;
	}
	
	/** Getters et Setters **/
	public ArrayList<String> getFiles() {
		return filesNames;
	}

	public void setFiles(ArrayList<String> files) {
		this.filesNames = files;
	}
	
	/**
	 * Fonction de chargement de tout les plugins de type AttackPlugin
	 * @return Une collection de AttackPlugins contenant les instances des plugins
	 * @throws Exception si file = null ou file.length = 0
	 */
	public ArrayList<AttackPlugins> loadAttackPlugin() throws Exception {
		
		this.initializeLoader();
		
		ArrayList<AttackPlugins> tmpPlugins = new ArrayList<AttackPlugins>();
		
		for(int index = 0 ; index < this.classAttackPlugins.size() ; index ++ ){
			
			//On cr�er une nouvelle instance de l'objet contenu dans la liste gr�ce � newInstance() 
			//et on le cast en GraphicPlugins.
			tmpPlugins.add( (AttackPlugins)((Class)this.classAttackPlugins.get(index)).newInstance() );			
		}
		
		return tmpPlugins;
	}
	
	/**
	 * Fonction de chargement de tout les plugins de type Graphic
	 * @return Une collection contenant les instances des plugins graphic
	 * @throws Exception si file = null ou file.length = 0
	 */
	public ArrayList<Class<?>> loadGraphicPlugins() throws Exception {
		
		this.initializeLoader();
		
		ArrayList<Class<?>> tmpPlugins = new ArrayList<Class<?>>();
		
		for(int index = 0 ; index < this.classGraphicPlugins.size() ; index ++ ){
			
			//On cr�er une nouvelle instance de l'objet contenu dans la liste gr�ce � newInstance() 
			//et on le cast en GraphicPlugins.
			tmpPlugins.add( (Class<?>) this.classGraphicPlugins.get(index).newInstance() );			
		}
		
		return tmpPlugins;
	}
	
	/**
	 * Fonction de chargement de tout les plugins de type MovementPlugin
	 * @return Une collection de MovementPlugins contenant les instances des plugins
	 * @throws Exception si file = null ou file.length = 0
	 */
	public ArrayList<MovementPlugins> loadMovementPlugin() throws Exception {
		
		this.initializeLoader();
		
		ArrayList<MovementPlugins> tmpPlugins = new ArrayList<MovementPlugins>();
		
		for(int index = 0 ; index < this.classMovementPlugins.size() ; index ++ ){
			
			//On cr�er une nouvelle instance de l'objet contenu dans la liste gr�ce � newInstance() 
			//et on le cast en GraphicPlugins.
			tmpPlugins.add( (MovementPlugins)((Class)this.classMovementPlugins.get(index)).newInstance() );			
		}
		
		return tmpPlugins;	}
	
	private void initializeLoader() throws Exception{
	
		//On v�rifie que la liste des plugins � charger a �t� initialis�e
		if(this.filesNames == null || this.filesNames.size() == 0 ){
			throw new Exception("Pas de fichier sp�cifi�");
		}		

		//Pour eviter le double chargement des plugins
		if(this.classAttackPlugins.size() != 0 || this.classGraphicPlugins.size() != 0 || this.classMovementPlugins.size() != 0 ){
			return ;
		}
		
		// --- On va charger les fichiers en m�moire ----
		
		File[] filesInstance = new File[this.filesNames.size()];
		//Pour charger le .jar en memoire
		URLClassLoader loader;
		//Pour la comparaison de chaines
		String tmp = "";
		//Pour le contenu de l'archive jar
		Enumeration enumeration;
		//Pour d�terminer quels sont les interfaces impl�ment�es
		Class<?> tmpClass = null;
		
		for(int index = 0 ; index < filesInstance.length ; index ++ ){
		
			// On recupere le nom du fichier et on cr�e une instance de la classe File pour manipuler ce fichier
			filesInstance[index] = new File(this.filesNames.get(index));
			
			if( !filesInstance[index].exists() ) {
				break;
			}
			
			URL u = filesInstance[index].toURI().toURL();
			//On cr�er un nouveau URLClassLoader pour charger le jar qui se trouve ne dehors du CLASSPATH
			loader = new URLClassLoader(new URL[] {u}); 
			
			//On charge le jar en m�moire
			JarFile jar = new JarFile("../PluginsRobotWar/plugins.jar");
			
			//On r�cup�re le contenu du jar
			enumeration = jar.entries();
			
			// On charge chaque fichier du jar
			while(enumeration.hasMoreElements()){
				
				tmp = enumeration.nextElement().toString();

				//On v�rifie que le fichier courant est un .class (et pas un fichier d'informations du jar )
				if(tmp.length() > 6 && tmp.substring(tmp.length()-6).compareTo(".class") == 0) {
					
					tmp = tmp.substring(0,tmp.length()-6);
					tmp = tmp.replaceAll("/",".");
					
					tmpClass = Class.forName(tmp ,true,loader);
					
					Annotation[] classAnnotations = tmpClass.getAnnotations();
					
					// On regarde les annotations de la classe
					for(Annotation annotation : classAnnotations){
						
						// C'est un plugin graphic
						if(annotation.getClass().equals(GraphicPlugin.class)){							
							this.classGraphicPlugins.add(tmpClass);
							
							// C'est un BasicGraphicPlugin
							/*if(((GraphicPlugin)annotation).pluginType().equals(EGraphicPluginsNames.BASIC_GRAPHIC)) {								
								this.classGraphicPlugins.add(tmpClass);								
							}*/
						}
						else if(annotation.getClass().equals(AttackPlugin.class)){
							this.classAttackPlugins.add(tmpClass);
						}
						else if(annotation.getClass().equals(MovePlugin.class)){
							this.classMovementPlugins.add(tmpClass);
						}
						
						/*
						//Une classe ne doit pas appartenir � deux cat�gories de plugins diff�rents. 
						//Si tel est le cas on ne la place que dans la cat�gorie de la premi�re annotation correcte
						// trouv�e
						if( ((Plugin)annotation).pluginType().equals(EPluginsNames.ATTACK_PLUGINS) ){
							this.classAttackPlugins.add(tmpClass);
						}
						// On a un seul type d'annotation Plugin, donc on peut caster sans soucis l'annotation en Plugin
						else if( ((Plugin)annotation).pluginType().equals(EPluginsNames.GRAPHIC_PLUGINS) ){
							
							this.classGraphicPlugins.add(tmpClass);

						}
						else if( ((Plugin)annotation).pluginType().equals(EPluginsNames.MOVE_PLUGINS) ){
							this.classMovementPlugins.add(tmpClass);
						}*/
					}
				}
			}
			jar.close();
		}		
	}
	
}
