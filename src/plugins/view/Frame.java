package plugins.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import plugins.BasicGraphicPlugin;
import plugins.GraphicPlugins;
import plugins.controller.PluginsController;
import plugins.loader.PluginsLoader;
import plugins.names.EGraphicPluginsNames;

/**
 * Cette classe permet de tester les plugins et aussi PluginsLoader
 *
 */

public class Frame extends JFrame implements ActionListener{

	private static final long serialVersionUID = 3320590018254342350L;

	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenu graphicPluginsMenu;
	
	private JMenuItem runPluginsMenuItem;
	private JMenuItem basicgraphicPluginMenuItem;

	private JTextArea graphicPluginsTextArea;
	
	private ArrayList<String> files;
	private ArrayList<Class<?>> graphicPlugins;
	private Class<BasicGraphicPlugin> basicGraphicInstance;
	
	private PluginsController plugController;
	
	
	public Frame(PluginsController plugController){
		this.plugController = plugController;

		this.files = new ArrayList<String>();
		// Remplir le tab "files"
		this.files.add("bin/plugins/BasicGraphicPlugin.class");
		this.graphicPlugins  = new ArrayList<Class<?>>();
		
		this.initialize();
	}


	private void initialize() {
		
		this.menuBar = new JMenuBar();
		this.fileMenu = new JMenu();
		this.graphicPluginsMenu = new JMenu();
		this.runPluginsMenuItem = new JMenuItem();
		this.basicgraphicPluginMenuItem = new JMenuItem();
		this.graphicPluginsTextArea = new JTextArea();

		//menuBar
		this.menuBar.add(this.fileMenu);
		this.menuBar.add(this.graphicPluginsMenu);
		
		//fileMenu
		this.fileMenu.setText("Fichier");
		this.fileMenu.add(this.runPluginsMenuItem);
		this.fileMenu.add(this.basicgraphicPluginMenuItem);
		this.fileMenu.addSeparator();

		//basiGraphicPluginMenu
		this.graphicPluginsMenu.setText(EGraphicPluginsNames.BASIC_GRAPHIC.getName());
		this.graphicPluginsMenu.addActionListener(this);
		
		//runPluginsMenuItem
		this.runPluginsMenuItem.setText("Lancer les plugins charger");
		this.runPluginsMenuItem.addActionListener(this);
		
		// BasicGMEnuItem
		this.basicgraphicPluginMenuItem.setText(EGraphicPluginsNames.BASIC_GRAPHIC.getName());
		this.basicgraphicPluginMenuItem.addActionListener(this);
		
		//stringTextArea
		this.graphicPluginsTextArea.setBorder(new LineBorder(Color.black));
		this.graphicPluginsTextArea.setText("Zone pour les plugins graphique");
		
		//this
		this.setSize(800,600);
		this.setJMenuBar(this.menuBar);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(2,1));
		this.getContentPane().add(this.graphicPluginsTextArea);
		
		
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("Frame.actionPerformed()");
		//if( arg0.getSource() == this.loadMenuItem ){
			
		if( this.runPluginsMenuItem == arg0.getSource() ){
			//this.pluginsLoader.setFiles(this.files);
			//this.plugController.
			System.out.println("runplugins");
			
			//this.loadAGraphicPlugin(EGraphicPluginsNames.BASIC_GRAPHIC);

			
			//this.loadAGraphicPlugin();
			
			try {
				//this.fillBasicGraphicPlugin(this.pluginsLoader.loadGraphicPlugin());
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		// si on selectionne un plugin a utiliser parmis les menu
		else {
			System.out.println("doAction");
			this.loadAGraphicPlugin(EGraphicPluginsNames.BASIC_GRAPHIC);

			
			//this.ActionFromPlugins(arg0);
		}
		
	}
	
	private void loadAGraphicPlugin(EGraphicPluginsNames pluginType){
		
		this.plugController.loadAGraphicPlugin(pluginType);
		
	}
	
	private void fillBasicGraphicPlugin(ArrayList<Class<?>> plugins){
		
		JMenuItem menuItem ;
			
		for(int index = 0 ; index < plugins.size(); index ++ ){
			this.graphicPlugins.add(plugins.get(index));
			
			menuItem = new JMenuItem();
			menuItem.setText(plugins.get(index).getLibelle() ); 
			menuItem.addActionListener(this);
			//Ajout dans la collection de JMenuItem pour d�tection du click
			//this.basicGraphicPluginsMenuItem.add(menuItem);
			//Ajout dans le menu
			this.graphicPluginsMenu.add(menuItem);
		}	
	}
	
	private void ActionFromPlugins(ActionEvent e){
		
		for(int index = 0 ; index < this.graphicPlugins.size(); index ++ )
		{
			if(e.getActionCommand().equals(EGraphicPluginsNames.BASIC_GRAPHIC.getName())){
				this.loadAGraphicPlugin(EGraphicPluginsNames.BASIC_GRAPHIC);
			}
			/*if(e.getActionCommand().equals( ((GraphicPlugins)this.graphicPlugins.get(index)).getLibelle() )){
				//this.basicGraphicPluginTextArea.setText(((GraphicPlugins)this.basicGraphicPlugins.get(index)).drawRobot(null));
				((GraphicPlugins)this.graphicPlugins.get(index)).drawRobot(null);
				this.graphicPluginsTextArea.setText("Coucou je suis un robot");

				return ;
			}*/
		}
	}
	
}
