package fr.unice.miage.moteur;

import java.util.concurrent.TimeUnit;

public class AnimeJeu extends Thread {
	boolean running = true;

	public void stopThread() {
		running = false;
	}

	public void run() {
		while (running) {
			try {
				TimeUnit.SECONDS.sleep(1);
				System.out.println("AnimeJeu.run()");
			} catch (InterruptedException e) {
				e.printStackTrace();
				running = false;
			}
		}
	}
}
