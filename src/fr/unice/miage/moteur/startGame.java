package fr.unice.miage.moteur;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

import fr.unice.miage.robot.Robot;

public class startGame extends JFrame {

	private static final long serialVersionUID = 1128540170538090826L;
	private static final int nbRobot = 2;

	private JFrame frame;
	private JPanel contentPane;
	ArrayList<Robot> tabRobots = new ArrayList<Robot>();

	public static void main(String[] args) {
		try {
			new startGame().showFrame();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void showFrame() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (frame == null) {
			frame = new JFrame("Robot War");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(0, 0));
			frame.setContentPane(contentPane);
		}
		buildMenu();
		frame.setVisible(true);
	}

	@SuppressWarnings("serial")
	void buildMenu() {
		JMenuBar bar = new JMenuBar();
		frame.setJMenuBar(bar);
		JMenu fileM = new JMenu("Fichier");
		bar.add(fileM);
		fileM.add(new AbstractAction("New Game", new ImageIcon("res/new-icon16.png")) {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				tabRobots.clear();
				beginGame();
			}

			@Override
			public Object getValue(String arg0) {
				if (arg0 == Action.ACCELERATOR_KEY)
					return KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK);
				return super.getValue(arg0);
			}
		});
	}

	void beginGame() {
		for (int i = 0; i < this.nbRobot; i++) {
			System.out.println("Robot n�" + i);
			tabRobots.add(new Robot());
			System.out.println(tabRobots.size());
		}
		new AnimeJeu().start();
	}

}
