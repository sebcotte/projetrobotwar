package fr.unice.miage.robot;

import java.awt.Color;
import java.awt.Graphics;

import fr.unice.miage.plugins.MovementPlugins;
import fr.unice.miage.visu.Champ;
import fr.unice.miage.visu.Dessinable;

public class Robot implements Runnable, MovementPlugins, Dessinable {

	private int health = 100;
	private int energy = 100;
	private double x, y;
	protected Color couleur;
	protected Champ champ;

	public Robot() {
		System.out.println("Robot.Robot()");
	}

	@Override
	public void run() {
		System.out.println("Robot.run()");
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	/* Implémentation de Plugins */

	@Override
	public String getLibelle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCategorie() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* Implémentation de MovementPlugin */

	@Override
	public void seDeplacer() {
		// TODO Auto-generated method stub

	}
	
	/* Implémentation de Positionnable */

	@Override
	public double getX() {
		return x;
	}

	@Override
	public void setX(double x) {
		this.x = x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public void setY(double y) {
		this.y = y;
	}

	@Override
	public Champ getChamp() {
		return champ;
	}

	/* Implémentation de Dessinable */

	@Override
	public Color getCouleur() {
		return couleur;
	}

	@Override
	public void seDessine(Graphics g) {
		int x = (int) this.x;
		int y = (int) this.y;
		
		g.setColor(couleur);
		g.fillRect(10, 20, 50, 50);

	}

}
