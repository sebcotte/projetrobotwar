package fr.unice.miage.plugins;

import java.awt.Graphics;

import plugins.annotations.GraphicPlugin;
import plugins.names.EGraphicPluginsNames;

/**
 * Ce plugin permet de repr�senter les robots par un carr� de couleur al�atoire
 *
 */

@GraphicPlugin(pluginType = EGraphicPluginsNames.BASIC_GRAPHIC)
public class BasicGraphicPlugin implements GraphicPlugins {
	
	public String getPluginType() {
		String type = "";
		this.getClass().getAnnotation();
		
		return 
	}

	@Override
	public String getLibelle() {
		return "Basic Graphic Plugin";
	}

	@Override
	public int getCategorie() {
		return 0;
	}

	@Override
	public void drawRobot(Graphics g) {
		// On dessine un robot sous forme de carr� de couleur al�atoire
		System.out.println("Je dessine un robot");
	}

}
