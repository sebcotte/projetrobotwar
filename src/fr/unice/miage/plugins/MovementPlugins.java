package fr.unice.miage.plugins;

import fr.unice.miage.plugins.Plugins;

@Plugin(libelle="movement", categorie="0")
public @interface MovementPlugins{

	/**
	 * Permet � un robot de se d�placer al�atoirement
	 */
	public void seDeplacer();
}
