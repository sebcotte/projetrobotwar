package fr.unice.miage.plugins;

import java.awt.Graphics;

public interface GraphicPlugins extends Plugins{

	/**
	 * Dessiner un robot � l'�cran
	 * @param g
	 */
	public void drawRobot(Graphics g);
}
