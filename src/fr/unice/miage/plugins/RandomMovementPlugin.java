package fr.unice.miage.plugins;

import java.util.Random;

/**
 * Ce plugin permet au robot de se d�placer de mani�re al�atoire
 *
 */
@MovementPlugins(libelle="random", categorie="movement")
public class RandomMovementPlugin implements MovementPlugin {

	@Override
	public String getLibelle() {
		return "Permet de d�placer le robot de mani�re al�atoire";
	}

	@Override
	public int getCategorie() {
		return 0;
	}

	@Override
	public void seDeplacer() {
		Random nb = new Random();
        return nb.nextInt(500) + 1;
	}

}
