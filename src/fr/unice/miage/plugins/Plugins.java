package fr.unice.miage.plugins;

/**
 * Cette interface repr�sente un plugin de base dans notre jeu
 * Chaque plugin doit impl�menter les m�thodes de cette interface
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Plugins {

	/**
	 * Obtient le libell� � afficher dans les menus ou autre pour le plugins
	 * @return Le libell� sous forme de String. Ce libell� doit �tre clair et compr�hensible facilement 
	 */
	public String libelle();
	
	/**
	 * Obtient la cat�gorie du plugins. 
	 * Cette cat�gorie est celle dans laquelle le menu du plugins sera ajout� une fois charg�
	 * @return Un entier qui repr�sente la cat�gorie
	 */
	public int categorie();
}
