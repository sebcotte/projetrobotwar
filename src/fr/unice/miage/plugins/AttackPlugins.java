package fr.unice.miage.plugins;

import fr.unice.miage.plugins.Plugins;

/**
 * Interface qui représente les plugins d'attaque
 */

public interface AttackPlugins extends Plugins {

	/**
	 * Le robot qui utilise cette méthode attaque le robot en face de lui
	 */
	public void attackRobot();

}
