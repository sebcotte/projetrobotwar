package fr.unice.miage.visu;

import java.util.List;

public interface Champ {
	
	public int getLargeur();
	
	public int getHauteur();
	
	public List<? extends Positionnable> getPositionnables();

}
