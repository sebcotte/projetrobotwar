package fr.unice.miage.controller;

import fr.unice.miage.frame.FrameGame;

public class FrameController {

	PluginsController plugController;

	public FrameController() {
		plugController = new PluginsController();
	}

	public Object findAPlugin(String plugType, String plugName) {
		return plugController.findAPlugin(plugType, plugName);
	}

	/**
	 * Depart de l'application dans cette classe
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new FrameGame().showFrame();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
