package fr.unice.miage.controller;

import java.util.ArrayList;
import java.util.Observable;

import fr.unice.miage.frame.FrameGame;
import fr.unice.miage.loader.PluginsLoader;


public class PluginsController extends Observable{
	
	private PluginsLoader plugLoader;
	private FrameGame frame;
	//private IA ia;
	
	public PluginsController() {
		try {
			plugLoader = new PluginsLoader();

			//this.addObserver(frame);

			//ia = new IA(frame);		

			
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Indiquer a l'IA que le plugin en param a ete charge et demande par l'utilisateur
	 * @param plugName
	 * @return
	 */
	/*public PluginInfo notifierIA(PluginInfo plug){
		try {
			
			ia.addPluginInfo(plug);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/

	public ArrayList<Class<?>> getListPlugins(){
		return plugLoader.getListPlugins();
	}
	public Object findAPlugin(String plugType, String plugName) {
		return plugLoader.findAPlugin(plugType, plugName);		
		
	}
	

}

