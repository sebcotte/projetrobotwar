package fr.unice.miage.frame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.unice.miage.controller.PluginsController;
import fr.unice.miage.moteur.MoteurDeJeu;
import fr.unice.miage.moteur.PluginInfo;

public class FrameGame extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1128540170538090826L;

	PluginsController plugController;
	
	private MoteurDeJeu moteur;

	public FrameGame() {
		try {
			this.plugController = new PluginsController();
			ArrayList<Class<?>> listPl = plugController.getListPlugins();
			for (Class<?> p : listPl) {
				System.out.println("Classe chargée : " + p.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showFrame() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		JFrame f = new JFrame("RobotWar");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
		JButton start = new JButton();
		start.setText("Commencer");
		start.addActionListener(this);

		JMenuBar menu = new JMenuBar();
		JMenu menuAttack = new JMenu("Attack");
		JMenu menuGraphic = new JMenu("Graphic");
		JMenu menuMovement = new JMenu("Movement");

		menu.add(menuAttack);
		menu.add(menuGraphic);
		menu.add(menuMovement);

		JMenuItem m = new JMenuItem("Basic Attack");
		menuAttack.add(m);
		m = new JMenuItem("Energy");
		menuGraphic.add(m);
		m = new JMenuItem("Basic Graphic");
		m.addActionListener(this);
		menuGraphic.add(m);

		m = new JMenuItem("Wally Graphic");
		m.addActionListener(this);
		
		menuGraphic.add(m);
		m = new JMenuItem("Health");
		menuGraphic.add(m);
		m = new JMenuItem("RandomMovement");
		menuMovement.add(m);
		m = new JMenuItem("LuckyMovement");
		menuMovement.add(m);

		f.getContentPane().add(menu, BorderLayout.NORTH);

		this.moteur = new MoteurDeJeu(700, 700, 2);

		f.getContentPane().add(this.moteur, BorderLayout.CENTER);
		f.getContentPane().add(start, BorderLayout.SOUTH);

		f.pack();
		f.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("FrameGame.actionPerformed()");
		if(e.getSource().getClass().equals(JButton.class)) {
			JButton b = (JButton)e.getSource();
			if( b.getText().equals("Commencer") ) {
				this.moteur.lancement();
			}
		}
		else if(e.getSource().getClass().equals(JMenuItem.class)) {
			JMenuItem j = (JMenuItem)e.getSource();
			if( j.getText().equals("Wally Graphic") ){
				this.moteur.addGraphicPlugin(new PluginInfo("Graphic", "WallyGraphic"));
			}
			else if(j.getText().equals("Basic Graphic") ) {
				this.moteur.addGraphicPlugin(new PluginInfo("Graphic", "BasicGraphic"));
			}
		}
		
		
	}
}