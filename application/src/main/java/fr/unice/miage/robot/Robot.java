package fr.unice.miage.robot;

import java.awt.Color;

public class Robot {

	private int health = 100;
	private int energy = 100;
	private int x = 325, y = 300;
	private Color color;
	private boolean alive = true;

	public Robot(int x, int y) {
		this.x = x;
		this.y = y;
		this.setColor();
	}

	public int getHealth() {
		return health;
	}

	/* Cette methode sera utilise dans le plugin Attack */
	private void setHealth(int health) {
		this.health = health;
	}

	public int getEnergy() {
		return energy;
	}

	private void setEnergy(int energy) {
		this.energy = energy;
	}

	public void decrementHealth(int health) {
		health = ((this.health - health) < 0) ? 0 : this.health - health;
		this.health = health;
		if(this.health == 0 && this.alive) {this.alive = false;};
	}

	public void incrementHealth(int health) {
		if(this.health+health > 0 && !this.alive) {this.alive = true;};
		health = ((this.health + health) > 100) ? 100 : this.health + health;
		this.health = health;
	}

	public void decrementEnergy(int energy) {
		energy = ((this.energy - energy) < 0) ? 0 : this.energy - energy;
		this.energy = energy;
	}

	public void incrementEnergy(int energy) {
		energy = ((this.energy + energy) > 100) ? 100 : this.energy + energy;
		this.energy = energy;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	private void setColor() {
		int r = (int) Math.round(Math.random() * 255);
		int g = (int) Math.round(Math.random() * 255);
		int b = (int) Math.round(Math.random() * 255);

		this.color = new Color(r, g, b);
	}

	public Color getColor() {
		return this.color;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public int calculateDistanceWithRobot(Object unRobot) {
		return (int) Math.hypot(this.x - (int)(((Robot)unRobot).getX()), this.y - (int)(((Robot)unRobot).getY()));
	}
}
