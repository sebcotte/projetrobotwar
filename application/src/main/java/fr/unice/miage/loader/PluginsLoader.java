package fr.unice.miage.loader;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarFile;


public class PluginsLoader {
	
	URLClassLoader loader;
	ArrayList<Class<?>> plugClasses;
	Class<?> plugAnnot;
	
	public PluginsLoader() throws Exception{
		plugClasses = new ArrayList<Class<?>>();
		
		// URLClassLoader
		this.initializeClassLoader();
		
		// JarFile
		this.loadJarContent();
	}
	
	private void initializeClassLoader() {
		try {
			// Preparer l'URL vers le .jar
			File j = new File("../plugin/target/plugin-0.0.1-SNAPSHOT.jar");
			URL u = j.toURI().toURL();
			
			// Preparer le ClassLoader
			this.loader = new URLClassLoader(new URL[] {u});
			
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}
	
	public void loadJarContent() {		
		
		// On charge le jar en memoire
		JarFile jarFile = null;
		try {
			jarFile = new JarFile("../plugin/target/fr.unice.miage.plugin-0.0.1-SNAPSHOT.jar"); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// On recupere le contenu de l'archive jar
		Enumeration<?> jarContent = jarFile.entries();
		
		// On charge chaque fichier du jar
		while(jarContent.hasMoreElements()){
			String aJarFileName = jarContent.nextElement().toString();

			 if(aJarFileName.length() > 6 && aJarFileName.substring(aJarFileName.length()-6).compareTo(".class") == 0) {
				 aJarFileName = aJarFileName.substring(0, aJarFileName.length()-6);
				 aJarFileName = aJarFileName.replaceAll("/",".");

				 try {
					 
					 // On charge la classe et on l'ajoute dans une liste
					 Class<?> plug = Class.forName(aJarFileName, true, this.loader);
					this.plugClasses.add(plug);
					
					// On charge l'annotation a part
					if(plug.isAnnotation()) {
						this.plugAnnot = plug;
					}
					
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				}
			 }		
		}		
	}
	
	/**
	 * Renvoie une instance du plugin demande en parametre
	 * @param libelle Le Libelle de l'annotation
	 * @param categorie
	 * @return Une instance du plugin demande 
	 */
	public Object findAPlugin(String libelle, String plugName) {
		if(this.plugClasses.size() < 1){
			System.out.println("Pas de plugin charge en memoire !");
			return null;
		}
		
		// on cherche la classe qui a l'annotation de type "plugName" donne en param
		for(Class<?> aPlugin : plugClasses){
			Annotation[] pluginAnnotations = aPlugin.getAnnotations();

			// On regarde les annotations de la classe
			for(Annotation annot : pluginAnnotations){
				if(annot.annotationType().equals(this.plugAnnot)) {
					try {
						// On regarde la valeur du libelle de l'annotation courante						
						String plugLibelle = ""+ annot.getClass().getMethod("libelle").invoke(annot);
						// On compare le libelle de l'annotation courante avec le libelle mis en param
						// On retourne le bon plugin
						if(libelle.equals(plugLibelle) && plugName.equals(aPlugin.getSimpleName())) {
							return aPlugin.newInstance();
						}
						
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
						e.printStackTrace();
					}
				}
			}		
		}
		return null;
	}
	
	public ArrayList<Class<?>> getListPlugins() {
		return this.plugClasses;
	}

	public static void main(String[] args) {
		try {
			//PluginsLoader load = new PluginsLoader();
			
			//Object o = load.findAPlugin("Graphic", "Health", new );
			//System.out.println(o.getClass().getName());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}	


}
