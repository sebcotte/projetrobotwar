package fr.unice.miage.moteur;

import java.awt.Dimension;
import java.awt.Graphics;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;

import fr.unice.miage.controller.FrameController;
import fr.unice.miage.robot.Robot;

public class MoteurDeJeu extends JPanel implements Runnable {

	private static final long serialVersionUID = -7220441538446772075L;
	private static final int PAUSE = 100;
	private static final int NB_ROBOT = 4;

	private ArrayList<Robot> tabRobots = new ArrayList<Robot>();
	private Thread thread;
	Object plugin;
	FrameController frameController;
	Graphics g;
	ArrayList<PluginInfo> loadedPlugins;
	ArrayList<PluginInfo> loadedGraphicPlugins;
	PluginInfo mainGraphicPlugin;

	public MoteurDeJeu(int width, int height, int number) {
		frameController = new FrameController();
		setPreferredSize(new Dimension(width, height));
		for (int i = 0; i < MoteurDeJeu.NB_ROBOT; i++) {
			tabRobots.add(
					new Robot((int) ((Math.random() * (610 - 10)) + 10), (int) ((Math.random() * (650 - 70)) + 70)));
		}
		
		this.loadedGraphicPlugins = new ArrayList<PluginInfo>();
		this.loadedPlugins = new ArrayList<PluginInfo>();
		
		// On charge les plugins de bases qui dessinent des formes
		this.mainGraphicPlugin = new PluginInfo("Graphic", "BasicGraphic");
		this.loadedGraphicPlugins.add(new PluginInfo("Graphic", "BasicGraphic"));
		this.loadedGraphicPlugins.add(new PluginInfo("Graphic", "Energy"));
		this.loadedGraphicPlugins.add(new PluginInfo("Graphic", "Health"));
		
		// On charge les plugins de base qui ne dessinent pas de forme
		this.loadedPlugins.add(new PluginInfo("Movement", "RandomMovement"));
		this.loadedPlugins.add(new PluginInfo("Movement", "LuckyMovement"));
		this.loadedPlugins.add(new PluginInfo("Attack", "LongAttack"));

	}

	public void lancement() {
		System.out.println("AnimeJeu.lancement()");
		thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * Ajouter un plugin qui appartient a la categorie "Graphic"
	 * @param plugInfo
	 */
	public void addGraphicPlugin(PluginInfo plugInfo) {
		
		// Si on veut ajouter un nouveau plugin graphic principal
		if(!plugInfo.getPlugName().equals(this.mainGraphicPlugin.getPlugName())) {
			
			// Alors on supprime le plugin principal actuel dans la liste
			Iterator<PluginInfo> iter = this.loadedGraphicPlugins.iterator();
			while (iter.hasNext()) {
				PluginInfo p = iter.next();

			 // On a trouve le plugin principal
				if(p.getPlugName().equals(this.mainGraphicPlugin.getPlugName())) {
					iter.remove();
				}
			}
			
			// On met a jour le plugin principal
			this.mainGraphicPlugin = plugInfo;
			
			this.loadedGraphicPlugins.add(plugInfo);
			
		}
		// Le plugin qu'on veut ajouter exite deja ou c'est un plugin Energy, Health
		else {
			// On ne veut pas enlever Energy ou Health
			return;
		}		
		
	}

	
	/**
	 * Ajouter un plugin qui ne dessine pas de forme et qui n'est donc pas dans la categorie "Graphic"
	 * Ex de categorie : Attack, Movement
	 * @param plugInfo
	 */
	public void addLoadedPlugin(PluginInfo plugInfo) {
		
		this.loadedPlugins.add(plugInfo);
	}


	@Override
	public void run() {
				
		while (tabRobots.size() > 1) {
			for (Robot r : tabRobots) {
	
				try {
					System.out.println("Le robot commence son tour");
					
					// On utilise les plugins qui ne dessine pas 
					for(PluginInfo p : this.loadedPlugins) {
						
						String name = p.getPlugName();
						String category = p.getCategory();
						String methodName = "";
						
						plugin = frameController.findAPlugin(category, name);
						
						switch(name) {
							case "RandomMovement":
								methodName = "seDeplacer";
								plugin.getClass().getMethod(methodName, Graphics.class, Object.class).invoke(plugin, this.g, r);

								break;
								
							case "LuckyMovement":
								methodName = "findEnergy";
								plugin.getClass().getMethod(methodName, Object.class).invoke(plugin, r);

								break;
								
							case "LongAttack":
								methodName = "attackRobot";								
								plugin.getClass().getMethod(methodName, Graphics.class, ArrayList.class, Object.class).invoke(plugin, this.g, tabRobots, r);
								
							// etc
						}		

					}
				
					System.out.println("Le robot a termine son tour");

						Thread.sleep(MoteurDeJeu.PAUSE);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException | InterruptedException e) {
					e.printStackTrace();
				}
				
				repaint();
			}
			// On supprime les robots morts du jeu
			for (Iterator<Robot> iterator = tabRobots.iterator(); iterator.hasNext();) {
				Robot robot = (Robot) iterator.next();
				if (robot.getHealth() == 0) {
					iterator.remove();
				}
			}
		}
		try {
			Thread.sleep(MoteurDeJeu.PAUSE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\n\n-------------- Partie terminée --------------");
		System.out.println("\n------------- Vainqueur du Robot ------------");
		System.out.println("\n----- Avec " + tabRobots.get(0).getHealth() + " de points de vie restants -----");
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		this.g = g;
		for (Robot r : tabRobots) {
			try {
				// On utilise les methodes des plugins ici
				// On charge les plugins de base
				
				// On utilise les plugins qui dessine des robots ou autres formes
				for(PluginInfo p : this.loadedGraphicPlugins) {
										
					String name = p.getPlugName();
					String category = p.getCategory();
					String methodName = "";
					
					// On charge que les plugins qui dessinent
					if(category.equals("Graphic")) {
						
						if(name.equals("WallyGraphic") || name.equals("BasicGraphic")) {
							methodName = "drawRobot";
						}
						else if(name.equals("Energy")) {
							methodName = "drawEnergy";
						}
						else if(name.equals("Health")) {
							methodName = "drawHealth";
						}
						
						plugin = frameController.findAPlugin(category, name);
						plugin.getClass().getMethod(methodName, Graphics.class, Object.class).invoke(plugin, this.g, r);
					}
					
				}
				
			} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException
					| NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
	}

	

}
