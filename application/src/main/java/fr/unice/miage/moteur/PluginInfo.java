package fr.unice.miage.moteur;

public class PluginInfo {
	
	String category;
	String plugName;
	
	public PluginInfo(String cat, String name) {
		category = cat;
		plugName = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPlugName() {
		return plugName;
	}

	public void setPlugName(String plugName) {
		this.plugName = plugName;
	}

}
